package com.sword.signature.tezos.reader.tzindex.model

import com.fasterxml.jackson.annotation.JsonProperty

data class TzContract(
    @JsonProperty("address")
    val address: String,
    @JsonProperty("creator")
    val creator: String,
    @JsonProperty("first_seen")
    val height: Long,
    @JsonProperty("bigmaps")
    val bigMaps : Map<Int, Long>
)
