package com.sword.signature.business.model.mail

import com.sword.signature.business.model.Account
import org.thymeleaf.context.Context

class ResetPasswordMail(
    recipient: Account,
    private val link: String
) : TransactionalMail(
    recipient = recipient,
    templateName = "reset_password_mail",
    subject = subjectTag + "Forgot your password ?"
) {
    override fun getContext() = Context().apply {
        setVariable("name", recipient.fullName)
        setVariable("link", link)
    }
}
