[Index](../README.md) | [Architecture](./architecture.md) | [Smart contract](../contract/README.md) | [Docker Deployment](./docker-deployment.md) | [Quick Start](./quickstart.md) | [Keys Management](./keys-management.md)

# Tezos keys management

## Keys handling using Tezos Client

* List all known keys
```shell
tezos-client list known addresses
```

* Display address information for a given alias
```shell
# Display the hash and public key
tezos-client show address $ALIAS
# Display the hash, public key, and secret key
tezos-client show address $ALIAS -S
```

* Encrypt an unencrypted secret key
```shell
tezos-client encrypt secret key
```

* Generate a new pair of keys for a given alias
```shell
tezos-client gen keys $ALIAS
```

* Import a secret key as a given alias
```shell
# Import an unencrypted secret key
tezos-client import secret key $ALIAS unencrypted:edsk...
# Import an encrypted secret key
tezos-client import secret key $ALIAS encrypted:edesk...
# Import a key from a connected ledger
tezos-client import secret key $ALIAS "ledger://..."
# Import a key from a remote signer
tezos-client import secret key $ALIAS "tcp://IP:PORT/$HASH"
```

* Delete an account (only from the client)
```shell
# Delete an account whose secret key is unknown
tezos-client forget address 
# Delete an account whose secret key is known
tezos-client forget address $ALIAS --force
```

* Transfer $XTZ tz from account $ACC1 to $ACC2
```shell
tezos-client transfer $XTZ from $ACC1 to $ACC2
```

## Get tz from Tezos Faucet (only for testnets)

* Download a JSON account file from [Tezos Faucet](https://faucet.tzalpha.net/)

* Activate the account using (connection to a synced node required)
```shell
tezos-client activate account $ALIAS with $FILE
```

* Retrieve the information from the account
```shell
tezos-client show address $ALIAS -S
```

* Encrypt the unencrypted secret key. A prompt is displayed and you have provide the unencrypted key with a custom password
```shell
tezos-client encrypt secret key
```

## Encrypt secret key for Digisign config file

Digisign works with unencrypted secret keys. However it's possible to encrypt those secret keys using Jasypt.
Note that any property within the config file can be encrypted that way.

* Retrieve the Jasypt binaries from [Github](https://github.com/jasypt/jasypt/releases/download/jasypt-1.9.3/jasypt-1.9.3-dist.zip)
`wget https://github.com/jasypt/jasypt/releases/download/jasypt-1.9.3/jasypt-1.9.3-dist.zip`

* Unzip the Jasypt ZIP 
`unzip jasypt-1.9.3-dist.zip`

* Update execution rights
`sudo chmod -R +x jasypt-1.9.3`

* To encrypt a secret key
`jasypt-1.9.3/bin/encrypt.sh input=edsk... password=$PASSWORD algorithm=PBEWITHHMACSHA512ANDAES_256 ivGeneratorClassName=org.jasypt.iv.RandomIvGenerator`

* To decrypt a secret key
`jasypt-1.9.3/bin/decrypt.sh input=HmmX6o6... password=$PASSWORD algorithm=PBEWITHHMACSHA512ANDAES_256 ivGeneratorClassName=org.jasypt.iv.RandomIvGenerator`

* Put the secret key and the public key in the compose-config/delphinet/application-daemon.yml configuration file.
```shell
 keys:
    admin:
      publicKey: "TO_REPLACE_WITH_YOUR_PUBLIC_KEY" # To replace with your public key.
      secretKey: "TO_REPLACE_WITH_YOUR_SECRET_KEY" # To replace with your secret key or with ENC(<encrypted secret key>)
      #secretKey: "ENC(XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX)"
      #secretKey: "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
```

* The password **\$PASSWORD** can be set in the environment variable **JASYPT_ENCRYPTOR_PASSWORD** or directly in the application-daemon.yml file.
```shell
jasypt:
  encryptor:
    password: $PASSWORD # Password for configuration passwords encryption.
    algorithm: PBEWITHHMACSHA512ANDAES_256
    iv-generator-classname: org.jasypt.iv.RandomIvGenerator
```
